package com.homework.exec;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

/**
 * Created by Valeriy on 07.11.2016.
 */
public class ExecutorsMatrix {
        static int firstMatrix[][] = new int[200][200];
        static int secondMatrix[][] = new int[200][200];
        static int resultMatrix[][] = new int[firstMatrix.length][secondMatrix[0].length];

        public static void main(String[] args) throws InterruptedException, IOException, ExecutionException {
            fillRandom(firstMatrix);
            fillRandom(secondMatrix);

            // Последовательное выполнение
            long start = System.currentTimeMillis();
            resultMatrix=sequentialCalculation(firstMatrix, secondMatrix, 0, 200);
            long time = System.currentTimeMillis() - start;
            System.out.println("Sequential calculation took: " + time + " milliseconds.");
            writeToFile(resultMatrix, "1.txt");


            // Параллельное выполнение на двух потоках
            start = System.currentTimeMillis();
            parallelCalculation(firstMatrix, secondMatrix, 2);
            time = System.currentTimeMillis() - start;
            System.out.println("Parallel calculation (2) took: " + time + " milliseconds.");


//            Параллельное выполнение на 4 потоках
            start = System.currentTimeMillis();
            parallelCalculation(firstMatrix, secondMatrix, 4);
            time = System.currentTimeMillis() - start;
            System.out.println("Parallel calculation (4) took: " + time + " milliseconds.");

        }

        public static void fillRandom(int[][] matrix) {
            Random rnd = new Random();
            for (int i = 0; i < matrix.length; i++)
                for (int j = 0; j < matrix[0].length; j++) {
                    matrix[i][j] = rnd.nextInt(9);}

        }

        private static void parallelCalculation(int[][] firstMatrix,
                                                int[][] secondMatrix,
                                                int threadCount) throws InterruptedException, ExecutionException, IOException {

            //число элементов, которые будут расчитаны каждым потоком
            final int partSize = resultMatrix[0].length / threadCount;

            ExecutorService service = Executors.newCachedThreadPool();
            List<Future<Integer[][]>> tasks = new ArrayList<>(threadCount);
                for (int i = 0; i < threadCount; i++) {
                    final int finalI = i;
                    tasks.add(service.submit(() -> {
                        Integer[][] newArray;
                            int from = finalI * partSize;
                            int[][] oldArray = sequentialCalculation(firstMatrix, secondMatrix, from, from + partSize);
                            newArray = new Integer[oldArray.length][oldArray[0].length];
                            for (int j = 0; j < oldArray.length; j++) {
                                for (int k = 0; k < oldArray[0].length; k++) {
                                    newArray[j][k] = oldArray[j][k];
                                }
                            }
                    return newArray;
                    }));
                }
                for (int i = 0; i < threadCount; i++) {
                    service.execute((Runnable) tasks.get(i));
                        for (int j = 0; j < resultMatrix[0].length; j++) {
                            for (int k = i*partSize; k <i*partSize+partSize; k++) {
                                resultMatrix[j][k]=tasks.get(i).get()[j][k];
                            }
                        }
                        if(i==threadCount-1) {
                            writeToFile(resultMatrix, (threadCount) + ".txt");
                        }
            }
            service.shutdown();
        }

        private static int[][] sequentialCalculation(int[][] firstMatrix,
                                                  int[][] secondMatrix,
                                                  int from, int to) {
            int tempMatrix[][] = new int[firstMatrix.length][secondMatrix[0].length];
            for (int i = 0; i < tempMatrix.length; i++) {
                for (int j = from; j < to; j++) {
                    tempMatrix[i][j] = calculateMultipleValue(getRow(i, firstMatrix), getCol(j, secondMatrix));
                }
            }
            return tempMatrix;
        }

        //вытаскивает всю строку по номеру
        private static int[] getRow(int rowNumber, int[][] matrix) {
            int[] row = new int[matrix[0].length];
            System.arraycopy(matrix[rowNumber], 0, row, 0, matrix[0].length);
            return row;
        }

        //вытаскивает весь столбец по номеру
        private static int[] getCol(int colNumber, int[][] matrix) {
            int[] col = new int[matrix.length];
            for (int i = 0; i < matrix.length; i++)
                col[i] = matrix[i][colNumber];
            return col;
        }

        private static void writeToFile(int[][] matrix, String fileName) throws IOException {
            BufferedWriter fileWriter = new BufferedWriter(new FileWriter(fileName));
            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    fileWriter.write(matrix[i][j] + " ");
                }
                fileWriter.newLine();
            }
            fileWriter.close();
        }

        private static int calculateMultipleValue(int[] row, int[] col) {
            int result = 0;
            //перемножаем соответствующие элементы и складываем
            for (int i = 0; i < row.length; i++)
                result += row[i] * col[i];
            return result;
        }

    }
